<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Project\Tools\Modules;

Loader::includeModule('tm.portal');
IncludeModuleLangFile(__FILE__);

class project_vote extends CModule {

    public $MODULE_ID = 'project.vote';

    use Modules\Install;

    function __construct() {
        $this->setParam(__DIR__, 'PROJECT_VOTE');
        $this->MODULE_NAME = Loc::getMessage('PROJECT_VOTE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_VOTE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_VOTE_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('PROJECT_VOTE_PARTNER_URI');
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        Modules\Utility::createDbTable('Project\Vote\Model\VoteTable');
    }

    public function UnInstallDB() {

    }

}
