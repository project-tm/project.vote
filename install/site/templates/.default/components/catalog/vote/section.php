<?

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;

Loader::includeModule('project.vote');
switch (Project\Vote\Start::getStatus($arParams['IBLOCK_ID'], $arResult['VARIABLES']['SECTION_CODE'])) {
    case 'start':
        $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
            'AREA_FILE_SHOW' => 'file',
            'PATH' => SITE_DIR . 'vote/include/start.php',
        ));
        break;

    case 'process':
        $APPLICATION->IncludeComponent(
                'project.catalog:section', 'vote', array(
            'COMPONENT_TEMPLATE' => '.default',
            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
            'SORT_BY1' => $arParams['ELEMENT_SORT_FIELD'],
            'SORT_ORDER1' => $arParams['ELEMENT_SORT_ORDER'],
            'SORT_BY2' => $arParams['ELEMENT_SORT_FIELD2'],
            'SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
            'FIELD_CODE' => $arParams['LIST_FIELD_CODE'],
            'PROPERTY_CODE' => $arParams['LIST_PROPERTY_CODE'],
            'AJAX_MODE' => 'N',
            'AJAX_OPTION_JUMP' => 'N',
            'AJAX_OPTION_STYLE' => 'N',
            'AJAX_OPTION_HISTORY' => 'N',
            'AJAX_OPTION_ADDITIONAL' => '',
            'CACHE_TYPE' => 'A',
            'CACHE_TIME' => '36000000',
            'CACHE_FILTER' => 'N',
            'CACHE_GROUPS' => 'Y',
            'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
            'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
            'INCLUDE_SUBSECTIONS' => 'Y',
            'COMPOSITE_FRAME_MODE' => 'A',
            'COMPOSITE_FRAME_TYPE' => 'AUTO',
            'DETAIL_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
                ), false
        );
        break;

    case 'stop':
        $APPLICATION->IncludeComponent('bitrix:main.include', '', array(
            'AREA_FILE_SHOW' => 'file',
            'PATH' => SITE_DIR . 'vote/include/stop.php',
        ));
        break;

    default:
        break;
}
