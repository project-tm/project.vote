<div class="ny_body_stop ny_stop">
    <div class="border_block">
        <div class="ny_stop__content">
            <div class="ny_stop__title">Благодарим Вас за выбор, коллеги!</div>
            <div class="ny_stop__text"><span class="winners_name">Шишкина Екатерина</span>, ждем Вас, на нашем празднике, <br>
                который состоится 28 декабря в 18:00<br>
                в городе Тула по ул.Октябрьской, 31 в ресторане <br>
                “Пряности и радости”
                <div class="center">
                    <a class="ny_start__next_step red_btn" href="<?= $APPLICATION->GetCurPage() ?>start/">нажмите просто так</a>
                </div>
            </div>
        </div>
    </div>
</div>