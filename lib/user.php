<?php

namespace Project\Vote;

use cUser,
    Project\Tools\Utility;

class User {

    public static function getList($arFilter) {
        return Utility\Cache::get(array(__CLASS__, __FUNCTION__, serialize($arFilter)), function() use($arFilter) {
                    $departament = array();
                    foreach (Config::DEPARTAMENT as $value) {
                        $departament = array_merge($departament, $value);
                    }
                    $arResult = array();
                    $rsUsers = CUser::GetList(($by = "name"), ($order = "asc"), $arFilter, array('FIELDS' => array('ID', 'LOGIN', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'), 'SELECT' => array('UF_DEPARTMENT')));
                    while ($arItem = $rsUsers->Fetch()) {
                        if (empty($arItem['NAME']) or in_array($arItem['ID'], Config::NO_USER)) {
                            continue;
                        }
                        if (strpos($arItem['NAME'], 'Обучение') === 0) {
                            continue;
                        }
                        $arUser = array(
                            'NAME' => $arItem['LAST_NAME'] . ' ' . $arItem['NAME'],
                            'PHOTO' => Utility\Image::resize($arItem['PERSONAL_PHOTO'], 220, 220, BX_RESIZE_IMAGE_EXACT),
                            'DEPARTMENT' => $arItem['UF_DEPARTMENT'],
                        );
                        $section = array();
                        foreach ($arItem['UF_DEPARTMENT'] as $dep) {
                            if (in_array($dep, $departament)) {
                                $section[] = 1;
                                break;
                            }
                        }
                        if (empty($section)) {
                            continue;
                        }
                        $arResult[$arItem['ID']] = $arUser;
                    }
                    return $arResult;
                }, 1);
    }

}
