<?php

namespace Project\Vote;

class Config {

    const DEPARTAMENT = array(
        'Отдел сопровождения' => array(
            3517, 4444, 4448
        ),
        'Тех поддержка' => array(
            37, 2795, 4516
        ),
        'Клиент менеджеры' => array(
            51
        ),
        'Телемаркетинг' => array(
            953
        ),
        'SEO' => array(
            35, 4426
        ),
        'Отдел продаж' => array(
            993,4628
        ),
    );
    const NO_USER = array(3, 27, 5);
    const IBLOCK_ID = 110;

}
