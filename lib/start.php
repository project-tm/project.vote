<?php

namespace Project\Vote;

use CIBlockElement,
    CIBlockFindTools,
    Bitrix\Main\Loader;

class Start {

    protected function get($iblockId, $code) {
        return $_SESSION[__CLASS__][$iblockId][$code] ?: 0;
    }

    protected function on($iblockId, $code) {
        $_SESSION[__CLASS__][$iblockId][$code] = true;
    }

    static public function getStatus($iblockId, $code) {
        if (self::isStop($iblockId, $code)) {
            return 'stop';
        }
        return self::get($iblockId, $code) ? 'process' : 'start';
    }

    static public function isStop($iblockId, $code) {
        $sectionId = CIBlockFindTools::GetSectionID(
                        0, $code, array(
                    'GLOBAL_ACTIVE' => 'Y',
                    'IBLOCK_ID' => $iblockId,
                        )
        );
        if ($sectionId) {
            $res = CIBlockElement::GetList(array(), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $iblockId, 'SECTION_ID' => $sectionId), false, false, array('ID'));
            $active = 0;
            while ($arItem = $res->Fetch()) {
                if (!Vote::status($sectionId, $arItem['ID'])) {
                    $active++;
                }
            }
            if(empty($active)) {
                unset($_SESSION[__CLASS__][$iblockId][$code]);
            }
            return (bool) !$active;
        }
        return false;
    }

    static public function isStart($iblockId, $arData) {
        if ($arData['ELEMENT_CODE'] === 'start') {
            self::on($iblockId, $arData['SECTION_CODE']);
            return true;
        } elseif (self::isStop($iblockId, $arData['SECTION_CODE'])) {
            return true;
        }
        return false;
    }

}
